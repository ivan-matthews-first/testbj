<?php

use Core\Config\Config;
use Core\Server\Server;
use Core\Router\Router;
use Core\Request\Request;
use Core\Session\Session;
use Core\Response\Response;
use Core\Template\Template;
use Core\Controller\Application;

require '../loader.php';

$config = new Config();
$config->loadConfigsFile();

$session = new Session();

$server = new Server($_SERVER);

$request = new Request();
$request->setRequestMethod($server->get('REQUEST_METHOD'));

$router = new Router($server->get('REQUEST_URI'));
$router->loadRoutesFile();
$router->findCurrentUriInRoutesList($request->getRequestMethod());

$response = new Response();

$application = new Application($request, $response, $router->getRouteParams());
$application->runApplication();

$response->sendHeaders();

$template = new Template($request, $response);
$template->run($request->headers()->get('accept'));