<?php

use Core\Template\Template;
use Helpers\Functions;
use Helpers\SortingPanel;
use Helpers\Pagination;

/**
 * @var Template $this
 * @var string $file
 * @var array $content
 * @var array $tasks
 * @var array $sortingKeys
 * @var array $sortingValues
 * @var string $currentSortingKey
 * @var string $currentSortingType
 * @var string $currentLink
 * @var integer $totalTasks
 * @var integer $limit
 * @var integer $offset
 * @var integer $logged
 */

$sort = new SortingPanel($currentSortingKey, $currentSortingType);
foreach ($sortingKeys as $key => $field) {
    $sort->key($key)->value($sortingValues[$key])->route('Home.index.sortable', $key);
}

$pagination = new Pagination($currentLink);
$pagination->total($totalTasks);
$pagination->limit($limit);
$pagination->offset($offset);
?>

<?php if (!$tasks) { ?>
    <div class="col-12 col-md-8 col-lg-6 mt-4">
        <div class="card">
            <div class="card-header">
                <h4>Нету существующих задач!</h4>
            </div>
            <div class="card-body">
                <h5 class="card-title">В Базе Данных не обнаружено ниодной задачи для показа.</h5>
                <p class="card-text">Если хотите, можете
                    <a href="<?php Functions::route('Home.createTask')->print() ?>">
                        создать новую задачу
                    </a>.
                </p>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-between">
                    <a href="<?php Functions::route('Home.createTask')->print() ?>"
                       class="btn btn-success">
                        Создать новую задачу
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php return false ?>
<?php } ?>

<?php $sort->print($this) ?>

    <div class="col-12">
        <div class="row">
            <?php foreach ($tasks as $task) { ?>
                <div class="card col-12 col-sm-6 col-lg-4">
                    <div class="card-body">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-9">
                                    <h5 class="card-title"><?php Functions::printString($task['userName']) ?></h5>
                                </div>
                                <?php if ($logged) { ?>
                                    <div class="col-1">
                                        <a href="<?php Functions::route('Home.editTask', $task['id'])->print() ?>"
                                           title="Изменить задачу">
                                            <i class="fas fa-edit text-warning"></i>
                                        </a>
                                    </div>
                                <?php } ?>
                                <?php if ($task['adminModified']) { ?>
                                    <div class="col-1" title="Задача изменена администратором">
                                        <i class="fas fa-user-edit text-primary"></i>
                                    </div>
                                <?php } ?>
                                <?php if ($task['taskStatus']) { ?>
                                    <div class="col-1" title="Задача выполнена">
                                        <i class="far fa-check-circle text-success"></i>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <h6 class="card-subtitle mb-2 text-muted"><?php Functions::printString($task['userEmail']) ?></h6>
                        <p class="card-text"><?php Functions::printString($task['taskBody']) ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

<?php $pagination->print($this) ?>