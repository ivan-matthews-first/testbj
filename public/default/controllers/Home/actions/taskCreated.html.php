<?php

use Core\Template\Template;
use Helpers\Functions;

/**
 * @var Template $this
 */
?>
<div class="col-12 col-md-8 col-lg-6 mt-4">
    <div class="card">
        <div class="card-header">
            <h4>Задача успешно создана!</h4>
        </div>
        <div class="card-body">
            <h5 class="card-title">Ваша задача была успешно создана.</h5>
            <p class="card-text">Ваша задача была успешно создана и добавлена в список</p>
            <p class="card-text">общих задач, который находится на
                <a href="<?php Functions::route('Home.index')->print() ?>">
                    главной странице сайта
                </a>.
            </p>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a href="<?php Functions::route('Home.index')->print() ?>"
                   class="btn btn-primary">
                    На главную
                </a>
                <a href="<?php Functions::route('Home.createTask')->print() ?>"
                   class="btn btn-success">
                    Создать еще одну задачу
                </a>
            </div>
        </div>
    </div>
</div>