<?php

use Core\Template\Template;
use Helpers\Template as TemplateHelper;

/**
 * @var Template $this
 */
?>
    <div class="col-12 mt-4 mb-2">
        <div class="row justify-content-center">
            <h2 class="col-12 col-md-8 col-lg-6">Редактирование задачи</h2>
        </div>
    </div>

<?php TemplateHelper::printForm($this) ?>