<?php

use Core\Template\Template;
use Helpers\Functions;

/**
 * @var Template $this
 * @var string $currentSortingKey
 * @var string $currentSortingType
 * @var array $values
 * @var array $routes
 */

?>
<div class="col-12">
    <ul class="nav nav-tabs nav-fill mt-2">
        <?php foreach ($values as $key => $value) { ?>
            <li class="nav-item">
                <?php if ($key === $currentSortingKey) { ?>
                    <a class="nav-link active" href="<?php Functions::printString($routes[$key]) ?>">
                        <?php Functions::printString($value) ?>
                        <?php if ($currentSortingType === 'desc') { ?>
                            <i class="fas fa-arrow-circle-up text-warning"></i>
                        <?php } else { ?>
                            <i class="fas fa-arrow-circle-down text-warning"></i>
                        <?php } ?>
                    </a>
                <?php } else { ?>
                    <a class="nav-link" href="<?php Functions::printString($routes[$key]) ?>">
                        <?php Functions::printString($value) ?>
                    </a>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
</div>