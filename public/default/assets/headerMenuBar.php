<?php

use Core\Template\Template;
use Helpers\Functions;
use Core\Session\Session;

/**
 * @var Template $this
 */

?>
<nav class="navbar navbar-light bg-dark">
    <div class="container-fluid justify-content-end">
        <?php if (!Session::user('id')) { ?>
            <a class="btn btn-primary me-2"
               href="<?php Functions::route('Users.auth')->print() ?>">Авторизация</a>
        <?php } else { ?>
            <a class="btn btn-primary me-2"
               href="<?php Functions::route('Users.exit')->print() ?>">Выход</a>
        <?php } ?>
        <a class="btn btn-success" href="<?php Functions::route('Home.createTask')->print() ?>">Создать
            новую задачу</a>
    </div>
</nav>