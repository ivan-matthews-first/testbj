<?php

use Core\Template\Template;
use Helpers\Form;

/**
 * @var Template $this
 * @var array $form
 * @var array $fields
 * @var array $options
 * @var array $errors
 */
?>
<div class="col-12">
    <div class="row justify-content-center">
        <form <?php Form::printFormAttributesString($form) ?>>
            <?php if (isset($errors['form'])) { ?>
                <div class="field-errors">
                    <?php Form::printFormErrors($errors['form']) ?>
                </div>
            <?php } ?>
            <?php foreach ($fields as $field => $fieldAttributes) { ?>
                <?php print $this->render($this->getCurrentThemeRootPath("assets/forms/fields/{$options[$field]['fieldType']}.html.php"), array(
                    'field' => $fieldAttributes,
                    'options' => isset($options[$field]) ? $options[$field] : array(),
                    'errors' => isset($errors[$field]) ? $errors[$field] : array(),
                )) ?>
            <?php } ?>
        </form>
    </div>
</div>