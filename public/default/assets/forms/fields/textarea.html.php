<?php

use Core\Template\Template;
use Helpers\Functions;
use Helpers\Form;

/**
 * @var Template $this
 * @var array $field
 * @var array $options
 * @var array $errors
 */

$value = $field['value'];
unset($field[$value]);

?>
<div class="form-group mt-2 mb-4">
    <?php if (isset($options['label'])) { ?>
        <label for="exampleInputEmail1"><?php Functions::printString($options['label']) ?></label>
    <?php } ?>
    <?php if (isset($errors)) { ?>
        <div class="field-errors">
            <?php Form::printFormErrors($errors) ?>
        </div>
    <?php } ?>
    <textarea <?php Form::printFormAttributesString($field) ?>><?php Functions::printString($value) ?></textarea>
    <?php if (isset($options['small'])) { ?>
        <small id="emailHelp" class="form-text text-muted"><?php Functions::printString($options['small']) ?></small>
    <?php } ?>
</div>