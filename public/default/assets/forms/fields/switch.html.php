<?php

use Core\Template\Template;
use Helpers\Functions;
use Helpers\Form;

/**
 * @var Template $this
 * @var array $field
 * @var array $options
 * @var array $errors
 */

?>
<div class="form-check form-switch">
    <?php if (isset($errors)) { ?>
        <div class="field-errors">
            <?php Form::printFormErrors($errors) ?>
        </div>
    <?php } ?>
    <input <?php Form::printFormAttributesString($field) ?><?php if ($field['value']) { ?> checked="checked"<?php } ?>>
    <?php if (isset($options['label'])) { ?>
        <label class="form-check-label"
               for="<?php Functions::printString($field['id']) ?>"><?php Functions::printString($options['label']) ?></label>
    <?php } ?>
    <?php if (isset($options['small'])) { ?>
        <small id="emailHelp" class="form-text text-muted"><?php Functions::printString($options['small']) ?></small>
    <?php } ?>
</div>