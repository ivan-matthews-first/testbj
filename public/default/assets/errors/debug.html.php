<?php

use Core\Template\Template;

/**
 * @var Template $this
 * @var string $file
 * @var array $content
 * @var string $errorNumber
 * @var string $errorHeader
 * @var string $errorMessage
 * @var string $errorFile
 * @var string $errorLine
 * @var array $backtrace
 */

/**
 * @param $errorFile
 * @param $errorLine
 * @return string
 */
$errorPreviewContent = function ($errorFile, $errorLine) {
    $resultContent = '<code><ol>';
    if (file_exists($errorFile)) {
        $content = file($errorFile);
        for ($i = $errorLine - 10; $i < $errorLine; $i++) {
            if (!isset($content[$i])) {
                continue;
            }
            $resultContent .= '<li value="' . ($i + 1) . '" class="content-line';
            if ($i + 1 === $errorLine) {
                $resultContent .= " bg-danger text-white error-string";
            }
            $resultContent .= '">';
            $resultContent .= htmlspecialchars($content[$i]);
            $resultContent .= '</li>';
        }
        for ($i = $errorLine; $i < $errorLine + 10; $i++) {
            if (!isset($content[$i])) {
                continue;
            }
            $resultContent .= '<li value="' . ($i + 1) . '" class="content-line';
            $resultContent .= '">';
            $resultContent .= htmlspecialchars($content[$i]);
            $resultContent .= '</li>';
        }
    }
    $resultContent .= '</ol></code>';
    return $resultContent;
}
?>
<link rel="stylesheet" href="<?php print $this->getCurrentThemeHttpPath('styles/debug.css') ?>">
<div class="error-body">
    <div class="error-header"><?php print $errorHeader ?></div>
    <div class="error-message">
        <pre><?php print $errorMessage ?></pre>
    </div>
    <div class="error-file-line">
        <div class="error-file"><?php print $errorFile ?></div>
        <div class="error-delimiter">,</div>
        <div class="error-line"><?php print $errorLine ?></div>
    </div>
    <?php if ($errorFile && $errorLine) { ?>
        <div class="error-content-preview">
            <pre><code><?php print $errorPreviewContent($errorFile, $errorLine) ?></code></pre>
        </div>
    <?php } ?>
    <div class="tracer-header">трассировка</div>
    <div class="error-backtrace">
        <ol class="accordion" id="debug-accordion">
            <?php foreach ($backtrace as $index => $trace) { ?>
                <li class="accordion-item">
                    <div class="backtrace-header accordion-header" id="heading<?php print $index ?>"
                         data-bs-toggle="collapse" data-bs-target="#collapse<?php print $index ?>" aria-expanded="true"
                         aria-controls="collapse<?php print $index ?>">
                        <div class="class-function accordion-button<?php if (!$trace['file'] || !$trace['line']) { ?> disabled<?php } ?>">
                            <div class="error-class"><?php print $trace['class'] ?></div>
                            <div class="error-type"><?php print $trace['type'] ?></div>
                            <div class="error-function-args">
                                <div class="error-function"><?php print $trace['function'] ?></div>
                                <div class="error-bracket">(</div>
                                <div class="error-args"></div>
                                <div class="error-bracket">)</div>
                            </div>
                        </div>
                        <?php if ($trace['file'] && $trace['line']) { ?>
                            <div class="error-file-line">
                                <div class="error-file"><?php print $trace['file'] ?></div>
                                <div class="error-delimiter">,</div>
                                <div class="error-line"><?php print $trace['line'] ?></div>
                            </div>
                        <?php } ?>
                    </div>
                    <?php if ($trace['file'] && $trace['line']) { ?>
                        <div class="backtrace-body accordion-collapse collapse<?php if (!$index) { ?> show<?php } ?>"
                             id="collapse<?php print $index ?>" aria-labelledby="heading<?php print $index ?>"
                             data-bs-parent="#debug-accordion">
                            <div class="backtrace-content-preview">
                                <pre><code><?php print $errorPreviewContent($trace['file'], $trace['line']) ?></code></pre>
                            </div>
                        </div>
                    <?php } ?>
                </li>
            <?php } ?>
        </ol>
    </div>
</div>
