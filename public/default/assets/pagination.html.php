<?php

use Core\Template\Template;
use Helpers\Functions;

/**
 * @var Template $this
 * @var integer $total
 * @var integer $limit
 * @var integer $offset
 * @var string $currentLink
 */

$nextOffset = $offset + $limit;
$prevOffset = $offset - $limit;

?>
<nav class="mt-4">
    <ul class="pagination pagination-md">
        <?php if ($prevOffset >= 0) { ?>
            <li class="page-item" aria-current="page">
                <a class="page-link" href="<?php Functions::uri($currentLink)->query(array(
                    'offset' => $prevOffset
                ))->mergeQuery()->print() ?>">
                    <span class="">Предыдущие <?php Functions::printString($limit, false) ?></span>
                </a>
            </li>
        <?php } else { ?>
            <li class="page-item disabled" aria-current="page">
                <span class="page-link">Предыдущие <?php Functions::printString($limit, false) ?></span>
            </li>
        <?php } ?>

        <?php if ($nextOffset < $total) { ?>
            <li class="page-item">
                <a class="page-link" href="<?php Functions::uri($currentLink)->query(array(
                    'offset' => $nextOffset
                ))->mergeQuery()->print() ?>">
                    <span class="">Следующие <?php Functions::printString($limit, false) ?></span>
                </a>
            </li>
        <?php } else { ?>
            <li class="page-item disabled">
                <span class="page-link">Следующие <?php Functions::printString($limit, false) ?></span>
            </li>
        <?php } ?>
    </ul>
</nav>