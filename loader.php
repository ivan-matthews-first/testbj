<?php

define('ROOT', __DIR__);
define('DEBUG_TIME', microtime(true));
define('DEBUG_MEMORY', memory_get_usage());

error_reporting(0);
ini_set('display_errors', '0');

date_default_timezone_set('Europe/London');

include_once ROOT . '/Core/ClassesAutoloader.php';

spl_autoload_register("\\Core\\ClassesAutoloader::loadClass");
set_error_handler("Core\\ErrorsHandler::errorHandler");
register_shutdown_function('Core\\ErrorsHandler::shutdownErrorHandler');

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}