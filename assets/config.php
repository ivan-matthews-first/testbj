<?php

use Core\Config\Config;

Config::database('db_host', 'localhost');
Config::database('db_port', 3306);
Config::database('db_user', 'root');
Config::database('db_pass', '123');
Config::database('db_name', 'test_db');
Config::database('db_charset', 'utf8mb4');
Config::database('db_collate', 'utf8mb4_unicode_ci');
Config::database('db_locale', 'ru_RU');
Config::database('db_engine', 'MyISAM');