<?php

use Core\Router\Router;

/**
 * @route-action Home.index \Controllers\Home\HomeController::index
 * @route-action Home.tasksList \Controllers\Home\HomeController::index
 * @route-action Home.index.sortable \Controllers\Home\HomeController::index
 * @route-action Home.createTask \Controllers\Home\HomeController::getCreateTaskForm
 * @route-action Home.createTask \Controllers\Home\HomeController::postCreateTaskForm
 * @route-action Home.editTask \Controllers\Home\HomeController::getEditTaskForm
 * @route-action Home.editTask \Controllers\Home\HomeController::postEditTaskForm
 * @route-action Users.auth \Controllers\Users\UsersController::getAuthForm
 * @route-action Users.auth \Controllers\Users\UsersController::postAuthForm
 */

Router::get('Home.index', '/')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('index')
    ->addRouteToRoutesList();

Router::get('Home.tasksList', '/tasks')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('index')
    ->addRouteToRoutesList();

Router::get('Home.index.sortable', '/tasks/sort-{string}')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('index')
    ->addRouteToRoutesList();

Router::get('Home.createTask', '/tasks/create')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('getCreateTaskForm')
    ->addRouteToRoutesList();

Router::post('Home.createTask', '/tasks/create')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('postCreateTaskForm')
    ->addRouteToRoutesList();

Router::get('Home.taskCreatedSuccessful', '/tasks/create/successful')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('taskCreatedSuccessful')
    ->addRouteToRoutesList();

Router::get('Home.editTask', '/tasks/id-{integer}/edit')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('getEditTaskForm')
    ->addRouteToRoutesList();

Router::post('Home.editTask', '/tasks/id-{integer}/edit')
    ->controller(\Controllers\Home\HomeController::class)
    ->action('postEditTaskForm')
    ->addRouteToRoutesList();

Router::get('Users.auth', '/auth')
    ->controller(\Controllers\Users\UsersController::class)
    ->action('getAuthorizeForm')
    ->addRouteToRoutesList();

Router::post('Users.auth', '/auth')
    ->controller(\Controllers\Users\UsersController::class)
    ->action('postAuthorizeForm')
    ->addRouteToRoutesList();

Router::get('Users.exit', '/auth/exit')
    ->controller(\Controllers\Users\UsersController::class)
    ->action('logOut')
    ->addRouteToRoutesList();