<?php

namespace Helpers;

use Core\Request\Data;
use Core\Response\Interfaces\Response;

class Uri
{
    private $uri = '/';

    private $query = array();

    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    public function query(array $query)
    {
        $this->query = $query;
        return $this;
    }

    public function mergeQuery()
    {
        $data = new Data();
        $this->query = array_merge($data->getGetData(), $this->query);
        return $this;
    }

    public function get()
    {
        if ($this->query) {
            $this->uri = $this->uri . '?' . http_build_query($this->query);
        }
        return $this->uri;
    }

    public function print()
    {
        return Functions::printString($this->get(), false);
    }

    public function redirect(Response $response, $redirectCode = 302)
    {
        $response->setResponseCode($redirectCode);
        $response->setHeader('location', $this->get());
        return $this;
    }
}