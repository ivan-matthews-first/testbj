<?php

namespace Helpers;

use Core\Template\Template;

class Pagination
{
    private $link;

    private $total;

    private $limit;

    private $offset;

    public function __construct($currentLink)
    {
        $this->link = $currentLink;
    }

    public function total($total)
    {
        $this->total = $total;
        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function offset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function render(Template $template)
    {
        if ($this->total <= $this->limit) {
            return null;
        }
        return $template->render($template->getCurrentThemeRootPath('assets/pagination.html.php'), array(
            'total' => $this->total,
            'limit' => $this->limit,
            'offset' => $this->offset,
            'currentLink' => $this->link
        ));
    }

    public function print(Template $template)
    {
        return Functions::printString($this->render($template), false);
    }
}