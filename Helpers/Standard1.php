<?php

namespace Helpers;

class Standard1
{
    public static function trim_r($strOrArr, $charList = " \t\n\r\0\x0B")
    {
        if (is_array($strOrArr)) {
            foreach ($strOrArr as $key => $value) {
                $strOrArr[$key] = self::trim_r($value, $charList);
            }
        } else {
            $strOrArr = trim($strOrArr, $charList);
        }
        return $strOrArr;
    }
}