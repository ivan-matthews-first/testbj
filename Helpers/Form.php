<?php

namespace Helpers;

class Form
{
    public static function makeFormAttributesString(array $formAttributes)
    {
        $formAttributesString = '';
        foreach ($formAttributes as $attribute => $value) {
            if (!$value) {
                continue;
            }
            $formAttributesString .= $attribute . '="' . Functions::getString($value) . '" ';
        }
        return rtrim($formAttributesString);
    }

    public static function renderFormErrors(array $errorsList)
    {
        $errorString = '';
        foreach ($errorsList as $error) {
            $errorString .= '<div class="form-error text-white bg-danger col-12 mb-1">';
            $errorString .= "<span class=\"p-2\">{$error}</span>";
            $errorString .= '</div>';
        }
        return $errorString;
    }

    public static function printFormErrors(array $errorsList)
    {
        return Functions::printString(self::renderFormErrors($errorsList), false);
    }

    public static function printFormAttributesString(array $formAttributes)
    {
        return Functions::printString(self::makeFormAttributesString($formAttributes), false);
    }
}