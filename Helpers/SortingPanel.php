<?php

namespace Helpers;

use Core\Template\Template;

class SortingPanel
{
    private $currentSortingKey;

    private $currentSortingType;

    private $key;

    private $values;

    private $routes;

    public function __construct($currentSortingKey, $currentSortingType)
    {
        $this->currentSortingKey = $currentSortingKey;
        $this->currentSortingType = strtolower($currentSortingType);
    }

    public function key($key)
    {
        $this->key = $key;
        return $this;
    }

    public function value($value)
    {
        $this->values[$this->key] = $value;
        return $this;
    }

    public function route($routeKey, ...$preparedData)
    {
        $this->routes[$this->key] = Functions::route($routeKey, ...$preparedData)->query(array(
            'sort' => $this->currentSortingKey === $this->key ?
                ($this->currentSortingType === 'desc' ? 'asc' : 'desc') : $this->currentSortingType
        ))->mergeQuery()->get();
        return $this;
    }

    public function render(Template $template)
    {
        return $template->render($template->getCurrentThemeRootPath('assets/sortingPanel.html.php'), array(
            'currentSortingKey' => $this->currentSortingKey,
            'currentSortingType' => $this->currentSortingType,
            'values' => $this->values,
            'routes' => $this->routes,
        ));
    }

    public function print(Template $template)
    {
        return Functions::printString($this->render($template), false);
    }
}