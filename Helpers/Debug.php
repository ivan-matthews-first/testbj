<?php

namespace Helpers;

class Debug
{
    public static function var_dump(...$_)
    {
        foreach ($_ as $value) {
            print '<pre>';
            var_dump($value);
            print '</pre>';
        }
    }

    public static function print_r(...$_)
    {
        foreach ($_ as $value) {
            print '<pre>';
            print_r($value);
            print '</pre>';
        }
    }

    public static function var_dump_die(...$_)
    {
        foreach ($_ as $value) {
            print '<pre>';
            var_dump($value);
            print '</pre>';
        }
        exit();
    }

    public static function print_r_die(...$_)
    {
        foreach ($_ as $value) {
            print '<pre>';
            print_r($value);
            print '</pre>';
        }
        exit();
    }
}