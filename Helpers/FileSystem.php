<?php

namespace Helpers;

class FileSystem
{
    public static function getFilePath($file)
    {
        $file = trim($file, '/');
        return ROOT . '/' . $file;
    }

    public static function getPhpFilePath($file)
    {
        $file = trim($file, '/');
        return ROOT . '/' . $file . '.php';
    }

    public static function getPublicRootPath($path)
    {
        return self::getFilePath('public/' . $path);
    }

    public static function getPublicHttpPath($path)
    {
        return 'public/' . $path;
    }
}