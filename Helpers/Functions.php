<?php

namespace Helpers;

use Core\Router\Builder;

class Functions
{
    public static function route($routeKey, ...$replacedData)
    {
        $link = '/';
        foreach (Builder::getRoutesList() as $routesList) {
            if (isset($routesList[$routeKey])) {
                $link = $routesList[$routeKey]['pattern'];
                break;
            }
        }
        if ($replacedData) {
            preg_match_all("#\{(.*?)\}#", $link, $resultMatches);
            $link = str_replace($resultMatches[0], $replacedData, $link);
        }
        return new Uri($link);
    }

    public static function uri($link)
    {
        return new Uri($link);
    }

    public static function getString($dataString, $prepare = true)
    {
        return $prepare ? htmlspecialchars($dataString) : $dataString;
    }

    public static function printString($dataString, $prepare = true)
    {
        print self::getString($dataString, $prepare);
        return null;
    }
}