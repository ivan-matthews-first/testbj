<?php

namespace Helpers;

use Core\Template\Template as TemplateClass;

class Template
{
    public static function renderForm(TemplateClass $template, $file = 'assets/forms/simpleForm.html.php')
    {
        $content = $template->getResponse()->getResponseContent();
        if (!isset($content['form'])) {
            return null;
        }
        return $template->render($template->getCurrentThemeRootPath($file), $content);
    }

    public static function printForm(TemplateClass $template, $file = 'assets/forms/simpleForm.html.php')
    {
        return Functions::printString(self::renderForm($template, $file), false);
    }
}