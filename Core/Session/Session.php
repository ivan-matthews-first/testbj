<?php

namespace Core\Session;
/**
 * Class Session
 * @package Core\Session
 * @method static user($key, $value = null)
 */
class Session
{
    public static function __callStatic($category, $arguments)
    {
        if (key_exists(1, $arguments)) {
            return self::set($category, $arguments[0], $arguments[1]);
        }
        return self::get($category, $arguments[0]);
    }

    public static function get($category, $key)
    {
        return isset($_SESSION[$category][$key]) ? $_SESSION[$category][$key] : null;
    }

    public static function set($category, $key, $value)
    {
        $_SESSION[$category][$key] = $value;
        return true;
    }

    public function __construct()
    {
        session_start();
    }
}