<?php

namespace Core\Config;

use Helpers\FileSystem;

/**
 * Class Config
 * @package Core\Config
 * @method static database($key, $value = null)
 */
class Config
{
    private static $configs = array();

    public static function __callStatic($name, $arguments)
    {
        if (key_exists(1, $arguments)) {
            return self::set($name, $arguments[0], $arguments[1]);
        }
        return self::get($name, $arguments[0]);
    }

    public static function get($category, $key)
    {
        return isset(self::$configs[$category][$key]) ? self::$configs[$category][$key] : null;
    }

    public static function set($category, $key, $value)
    {
        self::$configs[$category][$key] = $value;
        return true;
    }

    public function loadConfigsFile($configsFile = 'assets/config.php')
    {
        include_once FileSystem::getFilePath($configsFile);
        return $this;
    }
}