<?php

namespace Core\Controller;

use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;

class Application
{
    private $controllerParams = array();
    /** @var Request */
    private $request;
    /** @var Response */
    private $response;

    public function __construct(Request $request, Response $response, array $controllerParams)
    {
        $this->request = $request;
        $this->response = $response;
        $this->controllerParams = $controllerParams;
    }

    public function runApplication()
    {
        if (!$this->controllerParams || !$this->runController()) {
            $this->response->setResponseCode(404);
        }
        return $this;
    }

    protected function runController()
    {
        $controllerObject = new $this->controllerParams['controller']($this->request, $this->response);
        if ($this->response->getResponseCode() < 400) {
            return call_user_func_array(array($controllerObject, $this->controllerParams['action']), $this->controllerParams['arguments']);
        }
        return true;
    }
}