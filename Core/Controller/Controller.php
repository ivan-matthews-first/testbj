<?php

namespace Core\Controller;

use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;

class Controller
{
    /** @var Request */
    protected $request;
    /** @var Response */
    protected $response;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function __call($name, $arguments)
    {
        $this->response->setResponseCode(404);
    }

    public function setCurrentSortingType($sortingType)
    {
        return strtolower($sortingType) === 'desc' ? 'desc' : 'asc';
    }

    public function setOffset($offset)
    {
        return abs((int)$offset) ?: 0;
    }
}