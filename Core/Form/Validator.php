<?php

namespace Core\Form;

class Validator
{
    private $form;

    public function __construct(Form $form)
    {
        $this->form = $form;
    }

    protected function email($fieldName, $fieldOption, $fieldValue)
    {
        if ($fieldOption && $fieldValue != filter_var($fieldValue, FILTER_VALIDATE_EMAIL)) {
            $this->form->setFieldError($fieldName, 'Поле не является валидным E-MAIL адресом!');
        }
    }

    protected function required($fieldName, $fieldOption, $fieldValue)
    {
        if ($fieldOption && !$fieldValue) {
            $this->form->setFieldError($fieldName, 'Поле обязательно для заполнения!');
        }
        return $this;
    }

    protected function min($fieldName, $fieldOption, $fieldValue)
    {
        if (mb_strlen($fieldValue) < $fieldOption) {
            $this->form->setFieldError($fieldName, 'Значение поля слишком короткое!');
        }
        return $this;
    }

    protected function max($fieldName, $fieldOption, $fieldValue)
    {
        if (mb_strlen($fieldValue) > $fieldOption) {
            $this->form->setFieldError($fieldName, 'Значение поля слишком длинное!');
        }
        return $this;
    }

    public function validateForm()
    {
        foreach ($this->form->getFieldsOptions() as $field => $options) {
            foreach ($options as $method => $value) {
                if (method_exists($this, $method)) {
                    call_user_func(array($this, $method), $field, $value, $this->form->getFieldAttribute($field, 'value'));
                }
            }
        }
        return $this;
    }
}