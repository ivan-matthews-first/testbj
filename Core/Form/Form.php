<?php

namespace Core\Form;

use Core\Form\Traits\Fields;
use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;

class Form
{
    use Fields;

    /** @var Request */
    protected $request;
    /** @var Response */
    protected $response;

    private $field;

    private $attributes = array();

    private $options = array();

    private $errors = array();

    private $formAttributes = array();

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->setDefaultFormAttributes();
    }

    public function field($field)
    {
        $this->field = $field;
        $this->setFieldAttribute('name', $this->field);
        $this->setFieldAttribute('value', $this->request->get($this->field));
        return $this;
    }

    public function setFormAttribute($attribute, $value)
    {
        $this->formAttributes[$attribute] = $value;
        return $this;
    }

    public function getFormAttributes()
    {
        return $this->formAttributes;
    }

    public function setFieldError($field, $error)
    {
        $this->errors[$field][] = $error;
        return $this;
    }

    public function setFieldAttribute($attribute, $value)
    {
        $this->attributes[$this->field][$attribute] = $value;
        return $this;
    }

    public function setFieldOption($option, $value)
    {
        $this->options[$this->field][$option] = $value;
        return $this;
    }

    public function getFieldsErrors()
    {
        return $this->errors;
    }

    public function getFieldsAttributes()
    {
        return $this->attributes;
    }

    public function getFieldsOptions()
    {
        return $this->options;
    }

    public function getFieldErrors($field)
    {
        return $this->errors[$field];
    }

    public function getFieldAttribute($field, $attribute)
    {
        return $this->attributes[$field][$attribute];
    }

    public function getFieldOption($field, $option)
    {
        return $this->options[$field][$option];
    }

    private function setDefaultFormAttributes()
    {
        $this->setFormAttribute('class', 'col-12 col-md-8 col-lg-6');
        $this->setFormAttribute('method', 'POST');
        $this->setFormAttribute('accept-charset', 'UTF-8');
        $this->setFormAttribute('autocomplete', 'on');
        $this->setFormAttribute('enctype', 'application/x-www-form-urlencoded');
        return $this;
    }

    public function validateForm()
    {
        $validator = new Validator($this);
        $validator->validateForm();
        return $validator;
    }

    public function invalid()
    {
        return $this->errors;
    }

    public function valid()
    {
        return !$this->invalid();
    }

    public function renderForm()
    {
        $this->response->setContent('form', $this->formAttributes);
        $this->response->setContent('fields', $this->attributes);
        $this->response->setContent('options', $this->options);
        $this->response->setContent('errors', $this->errors);
        return $this;
    }
}