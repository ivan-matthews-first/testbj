<?php

namespace Core\Form\Traits;

trait Fields
{
    protected function submit()
    {
        $this->setFieldAttribute('name', null);
        $this->setFieldAttribute('value', null);
        $this->setFieldAttribute('type', 'submit');
        $this->setFieldAttribute('class', 'btn btn-primary');
        $this->setFieldOption('fieldType', 'submit');
        return $this;
    }

    protected function email()
    {
        $this->setFieldAttribute('type', 'email');
        $this->setFieldAttribute('class', 'form-control');
        $this->setFieldAttribute('required', true);
        $this->setFieldAttribute('placeholder', 'Введите корректный email-адрес');
        $this->setFieldOption('label', 'Введите корректный email-адрес');
        $this->setFieldOption('small', 'Корректным email-адресом считается адрес вида `simple.email@gmail.com`.');
        $this->setFieldOption('email', true);
        $this->setFieldOption('required', true);
        $this->setFieldOption('min', 5);
        $this->setFieldOption('max', 191);
        $this->setFieldOption('fieldType', 'email');
        return $this;
    }

    protected function password()
    {
        $this->setFieldAttribute('type', 'password');
        $this->setFieldAttribute('class', 'form-control');
        $this->setFieldAttribute('required', true);
        $this->setFieldAttribute('placeholder', 'Введите надежный пароль');
        $this->setFieldOption('label', 'Введите надежный пароль');
        $this->setFieldOption('small', 'Надежным паролем считается пароль длиной более 8 символов, состоящий из букв, цифр, символов смешанного регистра.');
        $this->setFieldOption('password', true);
        $this->setFieldOption('required', true);
        $this->setFieldOption('min', 3);
        $this->setFieldOption('max', 191);
        $this->setFieldOption('fieldType', 'password');
        return $this;
    }

    protected function text()
    {
        $this->setFieldAttribute('type', 'text');
        $this->setFieldAttribute('class', 'form-control');
        $this->setFieldAttribute('required', true);
        $this->setFieldAttribute('placeholder', 'Введите сюда что-нибудь');
        $this->setFieldOption('label', 'Введите сюда что-нибудь');
        $this->setFieldOption('small', null);
        $this->setFieldOption('text', true);
        $this->setFieldOption('required', true);
        $this->setFieldOption('min', 3);
        $this->setFieldOption('max', 191);
        $this->setFieldOption('fieldType', 'text');
        return $this;
    }

    protected function textarea()
    {
        $this->setFieldAttribute('type', 'textarea');
        $this->setFieldAttribute('class', 'form-control');
        $this->setFieldAttribute('required', true);
        $this->setFieldAttribute('placeholder', 'Заполните это поле');
        $this->setFieldOption('label', 'Заполните это поле');
        $this->setFieldOption('small', null);
        $this->setFieldOption('textarea', true);
        $this->setFieldOption('required', true);
        $this->setFieldOption('min', 6);
        $this->setFieldOption('max', 2000);
        $this->setFieldOption('fieldType', 'textarea');
        return $this;
    }

    protected function checkbox()
    {
        $this->setFieldAttribute('type', 'checkbox');
        $this->setFieldAttribute('class', 'form-check-input');
        $this->setFieldAttribute('required', true);
        $this->setFieldAttribute('id', 'flexSwitchCheckChecked');
        $this->setFieldAttribute('placeholder', 'Заполните это поле');
        $this->setFieldOption('label', 'Заполните это поле');
        $this->setFieldOption('small', null);
        $this->setFieldOption('checkbox', true);
        $this->setFieldOption('required', true);
        $this->setFieldOption('fieldType', 'checkbox');
        return $this;
    }
}