<?php

namespace Core;

class ClassesAutoloader
{
    public static function loadClass($namespace)
    {
        $class_path = str_replace('\\', '/', $namespace);
        $class_file = $class_path . '.php';
        if (file_exists($class_file)) {
            include_once ROOT . '/' . $class_file;
        }
        return;
    }
}

function file_exists($file)
{
    return \file_exists(ROOT . '/' . $file);
}