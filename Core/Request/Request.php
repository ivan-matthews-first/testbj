<?php

namespace Core\Request;

use Helpers\Standard1;
use Core\Request\Interfaces\Request as RequestInterface;

class Request implements RequestInterface
{
    private $requestData = array();

    private $requestMethod;

    private $headers;

    private $data;

    public function __construct()
    {
        $this->data = new Data();
        $this->setRequestData(array_replace_recursive($this->data->getRequestData(), $this->data->getPutData()));
        $this->headers = new Headers(getallheaders());
    }

    public function get($name)
    {
        return isset($this->requestData[$name]) ? $this->requestData[$name] : null;
    }

    public function set($name, $value)
    {
        $this->requestData[$name] = Standard1::trim_r($value);
        return $this;
    }

    public function drop($name)
    {
        if (isset($this->requestData[$name])) {
            unset($this->requestData[$name]);
        }
        return $this;
    }

    protected function setRequestData(array $requestData)
    {
        foreach ($requestData as $key => $value) {
            $this->set($key, $value);
        }
        return $this;
    }

    public function setRequestMethod($requestMethod)
    {
        $this->requestMethod = strtolower($requestMethod);
        return $this;
    }

    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    public function headers()
    {
        return $this->headers;
    }

    public function data()
    {
        return $this->data;
    }
}