<?php

namespace Core\Request\Interfaces;

use Core\Request\Data;
use Core\Request\Headers;

interface Request
{
    /**
     * @param $name
     * @return mixed
     */
    public function get($name);

    /**
     * @param $name
     * @param $value
     * @return self
     */
    public function set($name, $value);

    /**
     * @param $name
     * @return mixed
     */
    public function drop($name);

    /**
     * @param $requestMethod
     * @return self
     */
    public function setRequestMethod($requestMethod);

    /**
     * @return mixed
     */
    public function getRequestMethod();

    /**
     * @return Headers
     */
    public function headers();

    /**
     * @return Data
     */
    public function data();
}