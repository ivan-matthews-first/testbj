<?php

namespace Core\Request;

class Data
{
    public function __construct()
    {
    }

    public function getRequestData()
    {
        return $_REQUEST ? $_REQUEST : array();
    }

    public function getGetData()
    {
        return $_GET ? $_GET : array();
    }

    public function getPostData()
    {
        return $_POST ? $_POST : array();
    }

    public function getPutData()
    {
        $putData = file_get_contents('php://input');
        $putData = json_decode($putData, true);
        return $putData ? $putData : array();
    }
}