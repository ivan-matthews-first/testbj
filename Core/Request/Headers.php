<?php

namespace Core\Request;

use Helpers\Standard1;

class Headers
{
    private $headersList = array();

    public function __construct($headersRawList)
    {
        $this->setHeadersToHeadersList($headersRawList);
    }

    public function get($headerKey)
    {
        return isset($this->headersList[$headerKey]) ? $this->headersList[$headerKey] : null;
    }

    public function set($headerKey, $headerValue)
    {
        $this->headersList[strtolower($headerKey)] = Standard1::trim_r($headerValue);
        return $this;
    }

    public function drop($headerKey)
    {
        if (isset($this->headersList[$headerKey])) {
            unset($this->headersList[$headerKey]);
        }
        return $this;
    }

    public function setHeadersToHeadersList(array $headersList)
    {
        foreach ($headersList as $headerKey => $headerValue) {
            $this->set($headerKey, $headerValue);
        }
        return $this;
    }
}