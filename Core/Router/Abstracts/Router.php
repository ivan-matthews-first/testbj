<?php

namespace Core\Router\Abstracts;

use ReflectionMethod;

abstract class Router
{
    protected function findCurrentUriByMask($currentUri, $routesListSubArray)
    {
        foreach ($routesListSubArray as $routeParams) {
            preg_match("#{$this->preparePattern($routeParams['pattern'], $routeParams['mask'])}#", $currentUri, $success);
            if (isset($success[0]) && $success[0] === $currentUri) {
                $routeArguments = array_slice($success, 1);
                if ($this->countRequiredParams($routeParams['controller'], $routeParams['action'], count($routeArguments))) {
                    return array(
                        'controller' => $routeParams['controller'],
                        'action' => $routeParams['action'],
                        'arguments' => $routeArguments
                    );
                }
            }
        }
        return array();
    }

    protected function preparePattern($pattern, $mask)
    {
        $pattern = str_replace(array('{integer}', '{string}',), array('([0-9]+)', '([a-z]+)'), $pattern);
        return preg_replace("#\{(.*?)\}#", $mask, $pattern);
    }

    protected function countRequiredParams($controller, $action, $totalParams)
    {
        if (!method_exists($controller, $action)) {
            return false;
        }
        $reflectionMethod = new ReflectionMethod($controller, $action);
        return $reflectionMethod->getNumberOfRequiredParameters() <= $totalParams;
    }

    abstract public function loadRoutesFile($routesListFile = 'assets/router.php');

    abstract public function findCurrentUriInRoutesList($requestMethod);

    abstract public function getRouteParams();
}