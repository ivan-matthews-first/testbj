<?php

namespace Core\Router;

use Helpers\FileSystem;
use Core\Router\Abstracts\Router as AbstractionRouter;

/**
 * Class Builder
 * @package Core\Router
 * @method static |Builder any($routeKey, $routePattern)
 * @method static |Builder get($routeKey, $routePattern)
 * @method static |Builder post($routeKey, $routePattern)
 * @method static |Builder put($routeKey, $routePattern)
 * @method static |Builder delete($routeKey, $routePattern)
 * @method static |Builder patch($routeKey, $routePattern)
 * @method static |Builder options($routeKey, $routePattern)
 * @method static |Builder head($routeKey, $routePattern)
 * @method static |Builder connect($routeKey, $routePattern)
 * @method static |Builder trace($routeKey, $routePattern)
 */
class Router extends AbstractionRouter
{
    private $currentUri;

    private $routeParams;

    public static function __callStatic($name, $arguments)
    {
        $routeBuilder = new Builder();
        $routeBuilder->key($arguments[0]);
        $routeBuilder->pattern($arguments[1]);
        $routeBuilder->method($name);
        return $routeBuilder;
    }

    public function __construct($currentUri)
    {
        $this->currentUri = parse_url($currentUri, PHP_URL_PATH);
    }

    public function loadRoutesFile($routesListFile = 'assets/router.php')
    {
        include_once FileSystem::getFilePath($routesListFile);
        return $this;
    }

    public function findCurrentUriInRoutesList($requestMethod)
    {
        $routesList = Builder::getRoutesList();
        $this->routeParams = $this->findCurrentUriByMask($this->currentUri, $routesList['any']) ?:
            $this->findCurrentUriByMask($this->currentUri, $routesList[$requestMethod]);
        return $this;
    }

    public function getRouteParams()
    {
        return $this->routeParams;
    }
}