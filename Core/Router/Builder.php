<?php

namespace Core\Router;

class Builder
{
    private static $routesList = array(
        'any' => array(),
        'get' => array(),
        'post' => array(),
        'put' => array(),
        'delete' => array(),
        'patch' => array(),
        'options' => array(),
        'head' => array(),
        'connect' => array(),
        'trace' => array(),
    );

    private $routeOptions = array(
        'controller' => '',
        'action' => '',
        'pattern' => '',
        'mask' => '([a-z0-9_]+)',
        'method' => 'get',
    );

    private $routeKey;

    public function key($routeKey)
    {
        $this->routeKey = $routeKey;
        return $this;
    }

    public function controller($controller)
    {
        $this->routeOptions['controller'] = $controller;
        return $this;
    }

    public function action($action)
    {
        $this->routeOptions['action'] = $action;
        return $this;
    }

    public function pattern($pattern)
    {
        $this->routeOptions['pattern'] = $pattern;
        return $this;
    }

    public function mask($mask)
    {
        $this->routeOptions['mask'] = $mask;
        return $this;
    }

    public function method($method)
    {
        $this->routeOptions['method'] = strtolower($method);
        return $this;
    }

    public function addRouteToRoutesList()
    {
        self::$routesList[$this->routeOptions['method']][$this->routeKey] = $this->routeOptions;
        return;
    }

    public static function getRoutesList()
    {
        return self::$routesList;
    }
}