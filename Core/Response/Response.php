<?php

namespace Core\Response;

use Core\Response\Interfaces\Response as ResponseInterface;

class Response implements ResponseInterface
{
    private $responseCode = 200;

    private $responseStatus = '200 OK';

    private $headers = array();

    private $content = array();

    private $controller;

    private $action;

    private static $allowedResponseStatuses = array(
        100 => "100 Continue",
        101 => "101 Switching Protocol",
        200 => "200 OK",
        201 => "201 Created",
        202 => "202 Accepted",
        203 => "203 Non-Authoritative Information",
        204 => "204 No Content",
        205 => "205 Reset Content",
        206 => "206 Partial Content",
        300 => "300 Multiple Choices",
        301 => "301 Moved Permanently",
        302 => "302 Found",
        303 => "303 See Other",
        304 => "304 Not Modified",
        307 => "307 Temporary Redirect",
        308 => "308 Permanent Redirect",
        400 => "400 Bad Request",
        401 => "401 Unauthorized",
        403 => "403 Forbidden",
        404 => "404 Not Found",
        405 => "405 Method Not Allowed",
        406 => "406 Not Acceptable",
        407 => "407 Proxy Authentication Required",
        408 => "408 Request Timeout",
        409 => "409 Conflict",
        410 => "410 Gone",
        411 => "411 Length Required",
        412 => "412 Precondition Failed",
        413 => "413 Payload Too Large",
        414 => "414 URI Too Long",
        415 => "415 Unsupported Media Type",
        416 => "416 Range Not Satisfiable",
        417 => "417 Expectation Failed",
        418 => "418 I'm a teapot",
        422 => "422 Unprocessable Entity",
        425 => "425 Too Early",
        426 => "426 Upgrade Required",
        428 => "428 Precondition Required",
        429 => "429 Too Many Requests",
        431 => "431 Request Header Fields Too Large",
        451 => "451 Unavailable For Legal Reasons",
        500 => "500 Internal Server Error",
        501 => "501 Not Implemented",
        502 => "502 Bad Gateway",
        503 => "503 Service Unavailable",
        504 => "504 Gateway Timeout",
        505 => "505 HTTP Version Not Supported",
        511 => "511 Network Authentication Required",
        102 => '102 Processing',
        103 => '103 Early Hints',
        305 => '305 Use Proxy',
        306 => '306 Switch Proxy',
        402 => '402 Payment Required',
    );

    public function setResponseCode($responseCode)
    {
        if (isset(self::$allowedResponseStatuses[$responseCode])) {
            $this->responseCode = $responseCode;
            $this->responseStatus = self::$allowedResponseStatuses[$responseCode];
        }
        return $this;
    }

    public function setHeader($headerKey, $headerValue, $headerCode = null)
    {
        $this->headers[$headerKey] = array(
            'value' => $headerValue,
            'code' => $headerCode
        );
        return $this;
    }

    public function setContent($key, $value)
    {
        $this->content[$key] = $value;
        return $this;
    }

    public function getResponseCode()
    {
        return $this->responseCode;
    }

    public function getResponseStatus()
    {
        return $this->responseStatus;
    }

    public function getResponseHeaders()
    {
        return $this->headers;
    }

    public function getResponseContent()
    {
        return $this->content;
    }

    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function sendHeaders()
    {
        foreach ($this->headers as $headerKey => $header) {
            header("{$headerKey}: {$header['value']}", true, ($header['code'] ? $header['code'] : null));
        }
        http_response_code($this->responseCode);
        header("Status: {$this->responseStatus}", true, $this->responseCode);
        return $this;
    }
}