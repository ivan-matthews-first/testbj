<?php

namespace Core\Response\Interfaces;

interface Response
{
    /**
     * @param $responseCode
     * @return self
     */
    public function setResponseCode($responseCode);

    /**
     * @param $headerKey
     * @param $headerValue
     * @param null $headerCode
     * @return self
     */
    public function setHeader($headerKey, $headerValue, $headerCode = null);

    /**
     * @param $key
     * @param $value
     * @return self
     */
    public function setContent($key, $value);

    /**
     * @return self
     */
    public function getResponseCode();

    /**
     * @return self
     */
    public function getResponseStatus();

    /**
     * @return self
     */
    public function getResponseHeaders();

    /**
     * @return self
     */
    public function getResponseContent();

    /**
     * @param $controller
     * @return self
     */
    public function setController($controller);

    /**
     * @param $action
     * @return self
     */
    public function setAction($action);

    /**
     * @return string
     */
    public function getController();

    /**
     * @return string
     */
    public function getAction();

    /**
     * @return self
     */
    public function sendHeaders();
}