<?php

namespace Core\Database\Traits;

trait Getter
{
    public function getQueryOperation()
    {
        return $this->queryOperation;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getPreparedData()
    {
        return $this->preparedData;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function getJoinedTable()
    {
        return $this->joinTable;
    }

    public function getJoinedParam()
    {
        return $this->joinParam;
    }

    public function getJoinedType()
    {
        return $this->joinType;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getTableParams()
    {
        return $this->tableParams;
    }
}