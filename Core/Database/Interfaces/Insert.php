<?php

namespace Core\Database\Interfaces;

interface Insert
{
    /**
     * @param $field
     * @param $value
     * @return self|Result
     */
    public function insert($field, $value);
}