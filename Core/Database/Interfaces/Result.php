<?php

namespace Core\Database\Interfaces;

use Core\Database\Mysqli\Mysqli;
use Core\Database\Mysqli\Result as DatabaseResult;

interface Result
{
    /**
     * @param $prepareParam
     * @param $prepareData
     * @param $prepareType
     * @return self
     */
    public function prepare($prepareParam, $prepareData, $prepareType = Mysqli::STRING);

    /**
     * @return DatabaseResult
     */
    public function result();
}