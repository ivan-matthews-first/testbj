<?php

namespace Core\Database\Interfaces;

interface Table
{
    /**
     * @param array ...$fields
     * @return Operation|Result
     */
    public function select(...$fields);

    /**
     * @param $field
     * @param $value
     * @return Insert|Result
     */
    public function insert($field, $value);

    /**
     * @param $field
     * @param $value
     * @return Update|Operation|Result
     */
    public function update($field, $value);

    /**
     * @return Operation|Result
     */
    public function delete();

    /**
     * @return Result
     */
    public function truncate();

    /**
     * @return Result
     */
    public function alter();

    /**
     * @param $field
     * @param $params
     * @param string $index
     * @return Creator|Result
     */
    public function create($field, $params, $index = 'index');

    /**
     * @return Result
     */
    public function drop();
}