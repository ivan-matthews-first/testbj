<?php

namespace Core\Database\Interfaces;

interface Update
{
    /**
     * @param $field
     * @param $value
     * @return self|Operation|Result
     */
    public function update($field, $value);
}