<?php

namespace Core\Database\Interfaces;

interface Getter
{
    public function getQueryOperation();

    public function getTable();

    public function getPreparedData();

    public function getLimit();

    public function getOffset();

    public function getSort();

    public function getOrder();

    public function getJoinedTable();

    public function getJoinedParam();

    public function getJoinedType();

    public function getFields();

    public function getValues();

    public function getQuery();

    public function getTableParams();
}