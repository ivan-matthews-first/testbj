<?php

namespace Core\Database\Interfaces;

use Core\Database\Mysqli\Mysqli;
use Core\Database\Mysqli\Result;

interface Model
{
    /**
     * @return self
     */
    public function getConnection();

    /**
     * @param array ...$fields
     * @return self
     */
    public function select(...$fields);

    /**
     * @param $field
     * @param $value
     * @return self
     */
    public function insert($field, $value);

    /**
     * @param $field
     * @param $value
     * @return self
     */
    public function update($field, $value);

    /**
     * @return self
     */
    public function delete();

    /**
     * @return self
     */
    public function truncate();

    /**
     * @return self
     */
    public function alter();

    /**
     * @param $field
     * @param $params
     * @param string $index
     * @return self
     */
    public function create($field, $params, $index = 'index');

    /**
     * @return self
     */
    public function drop();

    /**
     * @param $table
     * @return self
     */
    public function table($table);

    /**
     * @param $table
     * @param $joiningParam
     * @param string $joinType
     * @return self
     */
    public function join($table, $joiningParam, $joinType = 'LEFT');

    /**
     * @param $query
     * @return self
     */
    public function query($query);

    /**
     * @param $limit
     * @param int $offset
     * @return self
     */
    public function limit($limit, $offset = 0);

    /**
     * @param $sort
     * @param string $order
     * @return self
     */
    public function sort($sort, $order = 'DESC');

    /**
     * @param $prepareParam
     * @param $prepareData
     * @param $prepareType
     * @return self
     */
    public function prepare($prepareParam, $prepareData, $prepareType = Mysqli::STRING);

    /**
     * @return Result
     */
    public function result();
}