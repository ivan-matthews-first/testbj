<?php

namespace Core\Database\Interfaces;

interface Creator
{
    /**
     * @param $field
     * @param $params
     * @param string $index
     * @return self|Result
     */
    public function create($field, $params, $index = 'index');
}