<?php

namespace Core\Database\Interfaces;

interface Operation
{
    /**
     * @param $table
     * @param $joiningParam
     * @param string $joinType
     * @return self|Result
     */
    public function join($table, $joiningParam, $joinType = 'LEFT');

    /**
     * @param $query
     * @return self|Result
     */
    public function query($query);

    /**
     * @param $limit
     * @param int $offset
     * @return self|Result
     */
    public function limit($limit, $offset = 0);

    /**
     * @param $sort
     * @param string $order
     * @return self|Result
     */
    public function sort($sort, $order = 'DESC');
}