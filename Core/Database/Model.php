<?php

namespace Core\Database;

use Core\Database\Mysqli\QueryBuilder;
use Core\Database\Mysqli\Connector;
use Core\Database\Mysqli\Mysqli;
use Core\Database\Traits\Getter;
use Core\Database\Interfaces\Table;
use Core\Database\Interfaces\Model as ModelInterface;
use Core\Database\Interfaces\Getter as GetterInterface;

class Model implements ModelInterface, GetterInterface, Table
{
    use Getter;

    protected $queryOperation;

    protected $table;

    protected $preparedData = array();

    protected $query;
    protected $limit;
    protected $offset;
    protected $sort;
    protected $order;

    protected $joinTable;
    protected $joinParam;
    protected $joinType;

    protected $fields = array();
    protected $values = array();
    protected $tableParams = array();

    /** @var Mysqli */
    private $database;

    /**
     * @param $table
     * @param $arguments
     * @return Table
     */
    public static function __callStatic($table, $arguments)
    {
        $selfObject = new self();
        $selfObject->getConnection();
        $selfObject->table($table);
        return $selfObject;
    }

    /**
     * @param $table
     * @param $arguments
     * @return Table
     */
    public function __call($table, $arguments)
    {
        $this->getConnection();
        $this->table($table);
        return $this;
    }

    public function __construct()
    {
    }

    public function getConnection()
    {
        $this->database = Connector::getConnection();
        return $this;
    }

    public function select(...$fields)
    {
        $this->queryOperation = __FUNCTION__;
        $this->fields = $fields;
        return $this;
    }

    public function insert($field, $value)
    {
        $this->queryOperation = __FUNCTION__;
        $this->values[$field] = $value;
        return $this;
    }

    public function update($field, $value)
    {
        $this->queryOperation = __FUNCTION__;
        $this->values[$field] = $value;
        return $this;
    }

    public function delete()
    {
        $this->queryOperation = __FUNCTION__;
        return $this;
    }

    public function truncate()
    {
        $this->queryOperation = __FUNCTION__;
        return $this;
    }

    public function alter()
    {
        $this->queryOperation = __FUNCTION__;
        return $this;
    }

    public function create($field, $params, $index = 'index')
    {
        $this->queryOperation = __FUNCTION__;
        $this->tableParams[] = array(
            'field' => $field,
            'params' => $params,
            'index' => $index,
        );
        return $this;
    }

    public function drop()
    {
        $this->queryOperation = __FUNCTION__;
        return $this;
    }

    public function table($table)
    {
        $this->table = $table;
        return $this;
    }

    public function join($table, $joiningParam, $joinType = 'LEFT')
    {
        $this->joinTable = $table;
        $this->joinParam = $joiningParam;
        $this->joinType = $joinType;
        return $this;
    }

    public function query($query)
    {
        $this->query = $query;
        return $this;
    }

    public function limit($limit, $offset = 0)
    {
        $this->limit = $limit;
        $this->offset = $offset;
        return $this;
    }

    public function sort($sort, $order = 'DESC')
    {
        $this->sort = $sort;
        $this->order = $order;
        return $this;
    }

    public function prepare($prepareParam, $prepareData, $prepareType = Mysqli::STRING)
    {
        $this->preparedData[] = array($prepareParam, $prepareData, $prepareType);
        return $this;
    }

    public function result()
    {
        $queryBuilder = new QueryBuilder($this);
        $query = $queryBuilder->makeQuery();

        $this->database->query($query);
        foreach ($this->preparedData as $preparedItem) {
            $this->database->prepare(...$preparedItem);
        }
        return $this->database->getResult();
    }
}