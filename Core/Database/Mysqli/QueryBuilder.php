<?php

namespace Core\Database\Mysqli;

use Core\Config\Config;
use Core\Database\Interfaces\Getter;

class QueryBuilder
{
    private $model;

    public function __construct(Getter $getter)
    {
        $this->model = $getter;
    }

    public function makeQuery()
    {
        return call_user_func(array($this, 'make' . $this->model->getQueryOperation() . 'Query'));
    }

    protected function makeSelectQuery()
    {
        $query = 'SELECT ';
        $query .= ($this->model->getFields() ? implode(', ', $this->model->getFields()) : '*') . ' ';
        $query .= 'FROM ';
        $query .= $this->model->getTable() . ' ';
        if ($this->model->getQuery()) {
            $query .= 'WHERE ';
            $query .= $this->model->getQuery() . ' ';
        }
        if ($this->model->getSort()) {
            $query .= 'ORDER BY ';
            $query .= $this->model->getSort() . ' ';
            $query .= $this->model->getOrder() . ' ';
        }
        if ($this->model->getLimit()) {
            $query .= 'LIMIT ';
            $query .= $this->model->getLimit() . ' ';
        }
        if ($this->model->getOffset()) {
            $query .= 'OFFSET ';
            $query .= $this->model->getOffset() . ' ';
        }
        $query = trim($query);
        $query .= ';';
        return $query;
    }

    protected function makeInsertQuery()
    {
        $query = 'INSERT INTO ';
        $query .= $this->model->getTable() . ' ';
        $query .= '(';
        $query .= '`' . implode('`, `', array_keys($this->model->getValues())) . '` ';
        $query .= ') ';
        $query .= 'VALUES ';
        $query .= '(';
        $query .= implode(', ', array_values($this->model->getValues()));
        $query .= ');';
        return $query;
    }

    protected function makeUpdateQuery()
    {
        $query = 'UPDATE ';
        $query .= $this->model->getTable() . ' ';
        $query .= 'SET ';
        $query .= $this->makeUpdateQueryString($this->model->getValues()) . ' ';
        if ($this->model->getQuery()) {
            $query .= 'WHERE ';
            $query .= $this->model->getQuery() . ' ';
        }
        if ($this->model->getSort()) {
            $query .= 'ORDER BY ';
            $query .= $this->model->getSort() . ' ';
            $query .= $this->model->getOrder() . ' ';
        }
        if ($this->model->getLimit()) {
            $query .= 'LIMIT ';
            $query .= $this->model->getLimit() . ' ';
        }
        if ($this->model->getOffset()) {
            $query .= 'OFFSET ';
            $query .= $this->model->getOffset() . ' ';
        }
        $query = trim($query);
        $query .= ';';
        return $query;
    }

    protected function makeDeleteQuery()
    {
        $query = 'DELETE FROM ';
        $query .= $this->model->getTable() . ' ';
        if ($this->model->getQuery()) {
            $query .= 'WHERE ';
            $query .= $this->model->getQuery() . ' ';
        }
        if ($this->model->getSort()) {
            $query .= 'ORDER BY ';
            $query .= $this->model->getSort() . ' ';
            $query .= $this->model->getOrder() . ' ';
        }
        if ($this->model->getLimit()) {
            $query .= 'LIMIT ';
            $query .= $this->model->getLimit() . ' ';
        }
        if ($this->model->getOffset()) {
            $query .= 'OFFSET ';
            $query .= $this->model->getOffset() . ' ';
        }
        $query = trim($query);
        $query .= ';';
        return $query;
    }

    protected function makeTruncateQuery()
    {
        $query = 'TRUNCATE TABLE ';
        $query .= $this->model->getTable();
        $query .= ';';
        return $query;
    }

    protected function makeAlterQuery()
    {
        $query = 'ALTER TABLE ';
        $query .= $this->model->getTable();
        $query .= $this->model->getQuery();
        $query .= ';';
        return $query;
    }

    protected function makeCreateQuery()
    {
        $query = 'CREATE TABLE IF NOT EXISTS ';
        $query .= $this->model->getTable();
        $query .= $this->makeCreateTableQueryString($this->model->getTableParams());
        $query .= 'ENGINE=' . Config::database('db_engine') . ' ';
        $query .= 'DEFAULT CHARSET=' . Config::database('db_charset') . ' ';
        $query .= 'COLLATE=' . Config::database('db_collate');
        $query .= ';';
        return $query;
    }

    protected function makeDropQuery()
    {
        $query = 'DROP TABLE IF EXISTS ';
        $query .= $this->model->getTable();
        $query .= ';';
        return $query;
    }

    private function makeUpdateQueryString(array $fields)
    {
        $queryString = '';
        foreach ($fields as $field => $value) {
            $queryString .= "`{$field}` = {$value}, ";
        }
        return rtrim($queryString, ', ');
    }

    private function makeCreateTableQueryString(array $tableParams)
    {
        $indexes = '';
        $query = '(';
        foreach ($tableParams as $param) {
            $query .= "`{$param['field']}` {$param['params']}, ";
            $indexes .= "{$param['index']} `{$param['field']}` (`{$param['field']}`), ";
        }
        $query = $query . rtrim($indexes, ', ') . ') ';
        return $query;
    }
}