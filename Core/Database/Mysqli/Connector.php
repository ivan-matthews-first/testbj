<?php

namespace Core\Database\Mysqli;

use Core\Config\Config;

class Connector
{
    /** @var \mysqli */
    private $connectionToDatabase;

    private static $instance;

    public static function getConnection($createNewConnection = false)
    {
        if (self::$instance === null || $createNewConnection) {
            self::$instance = new self();
        }
        return new Mysqli(self::$instance->connectionToDatabase);
    }

    private function __construct()
    {
        $this->newConnection();
        $this->setDatabaseDefaultParams();
    }

    public function __destruct()
    {
        $this->closeConnection();
    }

    private function newConnection()
    {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->connectionToDatabase = new \mysqli(Config::database('db_host'), Config::database('db_user'), Config::database('db_pass'), Config::database('db_name'), Config::database('db_port'));
        return $this;
    }

    private function closeConnection()
    {
        if ($this->connectionToDatabase) {
            $this->connectionToDatabase->close();
        }
        return $this;
    }

    private function setDatabaseDefaultParams()
    {
        $this->connectionToDatabase->set_charset(Config::database('db_charset'));
        $query = "SET ";
        $query .= "time_zone = '" . date('P') . "', ";
        $query .= "lc_messages = '" . Config::database('db_locale') . "', ";
        $query .= "default_storage_engine = " . Config::database('db_engine') . ", ";
        $query .= "default_tmp_storage_engine = " . Config::database('db_engine') . ";";
        $this->connectionToDatabase->query($query);
        return $this;
    }

}