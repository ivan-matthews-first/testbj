<?php

namespace Core\Database\Mysqli;

class Mysqli
{
    const INTEGER = 'i';
    const DOUBLE = 'd';
    const STRING = 's';
    const BLOB = 'b';

    /** @var \mysqli */
    private $mysqli;
    /** @var \mysqli_stmt */
    private $statement;

    private $queryString;

    private $preparedParamsValues = array();

    private $prepareKeys = array();

    private $prepareTypes;

    public function __construct(\mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function __destruct()
    {
        if ($this->statement instanceof \mysqli_stmt) {
            $this->statement->free_result();
            $this->statement->close();
        }
    }

    public function query($queryString)
    {
        $this->queryString = $queryString;
        return $this;
    }

    public function prepare($replaceableVariable, $prepareVariable, $prepareVariableType = Mysqli::STRING)
    {
        $this->preparedParamsValues[$replaceableVariable] = array(
            'stringToReplace' => $prepareVariable,
            'stringToReplaceType' => $prepareVariableType
        );
        return $this;
    }

    public function getResult()
    {
        $this->replaceQueryStringToPreparedString();
        $this->statement = $this->mysqli->prepare($this->queryString);
        if ($this->statement) {
            $this->setParamsToPreparedData();
            $this->statement->execute();
        }
        return new Result($this->mysqli, $this->statement);
    }

    private function replaceQueryStringToPreparedString()
    {
        if ($this->preparedParamsValues) {
            $this->queryString = preg_replace_callback("#:[a-zA-Z0-9]+#", function ($find) {
                if (isset($find[0]) && key_exists($find[0], $this->preparedParamsValues)) {
                    $this->prepareTypes .= $this->preparedParamsValues[$find[0]]['stringToReplaceType'];
                    $this->prepareKeys[] = $this->preparedParamsValues[$find[0]]['stringToReplace'];
                    return '?';
                }
                return $find[0];
            }, $this->queryString);
        }
        return $this;
    }

    private function setParamsToPreparedData()
    {
        if ($this->prepareTypes) {
            $this->statement->bind_param($this->prepareTypes, ...$this->prepareKeys);
        }
        return $this;
    }
}