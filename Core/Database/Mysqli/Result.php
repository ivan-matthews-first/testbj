<?php

namespace Core\Database\Mysqli;

use Core\StdClass;

class Result
{
    /** @var \mysqli */
    private $mysqli;
    /** @var \mysqli_stmt */
    private $statement;
    /** @var \mysqli_result */
    private $mysqliResult;

    public function __construct(\mysqli $mysqli, \mysqli_stmt $mysqli_stmt)
    {
        $this->mysqli = $mysqli;
        $this->statement = $mysqli_stmt;
        $this->mysqliResult = $this->statement->get_result();
    }

    public function __destruct()
    {
        if ($this->mysqliResult instanceof \mysqli_result) {
            $this->mysqliResult->free_result();
        }
    }

    public function one($asObject = true)
    {
        $result = array();
        if ($this->mysqliResult) {
            $result = $asObject ? $this->mysqliResult->fetch_object() : $this->mysqliResult->fetch_assoc();
        }
        return $result ? $result : ($asObject ? new StdClass() : array());
    }

    public function all($asObject = true)
    {
        $result = array();
        if ($this->mysqliResult) {
            while ($row = $asObject ? $this->mysqliResult->fetch_object() : $this->mysqliResult->fetch_assoc()) {
                $result[] = $row;
            }
        }
        return $result ? $result : ($asObject ? array(new StdClass()) : array());
    }

    public function rows()
    {
        return $this->mysqli->affected_rows;
    }

    public function id()
    {
        return $this->mysqli->insert_id;
    }
}