<?php

namespace Core;

use Core\Request\Request;
use Core\Response\Response;
use Core\Template\Template;

class ErrorsHandler
{
    /** @var Request */
    private $request;
    /** @var Response */
    private $response;
    /** @var Template */
    private $template;

    private $errorNumber, $errorMessage, $errorFile, $errorLine, $backtrace = array();

    private static $errorsMessages = array(
        '' => 'неизвестная ошибка  #%error_code%',
        0 => 'неизвестная ошибка',
        1 => 'критическая ошибка',
        2 => 'предупреждение',
        4 => 'синтаксическая ошибка',
        8 => 'уведомление',
        16 => 'ошибка уровня ядра',
        32 => 'предупреждение ядра',
        64 => 'ошибка компиляции',
        128 => 'предупреждение компиляции',
        256 => 'пользовательская ошибка',
        512 => 'пользовательское предупреждение',
        1024 => 'пользовательское уведомление',
        2048 => 'требование',
        4096 => 'восстановляемая ошибка',
        8192 => 'отклонение',
        16384 => 'пользовательское отклонение',
        32767 => 'неизвестная ошибка #%error_code%',
    );

    public static function errorHandler($errorNumber, $errorMessage, $errorFile, $errorLine)
    {
        $selfObject = new self($errorNumber, $errorMessage, $errorFile, $errorLine);
        $selfObject->render();
        return true;
    }

    public static function shutdownErrorHandler()
    {
        if (@is_array($e = @error_get_last())) {
            $code = isset($e['type']) ? $e['type'] : 0;
            $msg = isset($e['message']) ? $e['message'] : '';
            $file = isset($e['file']) ? $e['file'] : '';
            $line = isset($e['line']) ? $e['line'] : '';
            if ($code > 0) {
                return self::errorHandler($code, $msg, $file, $line);
            }
        }
        return false;
    }

    public function __construct($errorNumber, $errorMessage, $errorFile, $errorLine)
    {
        restore_error_handler();

        $this->request = new Request();
        $this->response = new Response();
        $this->template = new Template($this->request, $this->response);
        $this->errorNumber = $errorNumber;
        $this->errorMessage = $errorMessage;
        $this->errorFile = $errorFile;
        $this->errorLine = $errorLine;
        $this->backtrace = debug_backtrace();
    }

    protected function render()
    {
        $this->response->setResponseCode(500);
        $this->response->sendHeaders();
        $this->response->setController('../assets/errors');
        $this->response->setAction('../debug');

        $this->response->setContent('errorNumber', $this->errorNumber);
        $this->response->setContent('errorHeader', $this->getErrorMessageByErrorCode($this->errorNumber));
        $this->response->setContent('errorMessage', $this->errorMessage);
        $this->response->setContent('errorFile', $this->errorFile);
        $this->response->setContent('errorLine', $this->errorLine);
        $this->response->setContent('backtrace', $this->backtrace);

        $this->template->run($this->request->headers()->get('accept'));
        return exit();
    }

    protected function getErrorMessageByErrorCode($errorCode)
    {
        return isset(self::$errorsMessages[$errorCode]) ? self::$errorsMessages[$errorCode] : self::$errorsMessages[''];
    }
}