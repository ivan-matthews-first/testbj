<?php

namespace Core\Template\Traits;

use Core\Response\Response;

/**
 * Trait Render
 * @package Core\Template\Traits
 * @property Response $response
 */
trait Render
{
    protected function renderHtmlContent()
    {
        header('Content-Type: text/html');
        $this->renderContent();
        $this->renderTemplateBody();
        return $this;
    }

    protected function renderJsonContent()
    {
        header('Content-Type: application/json');
        print json_encode(array(
            'code' => $this->response->getResponseCode(),
            'status' => $this->response->getResponseStatus(),
            'headers' => $this->response->getResponseHeaders(),
            'content' => $this->response->getResponseContent()
        ));
        return $this;
    }

    protected function renderTemplateBody()
    {
        include_once $this->getCurrentThemeRootPath('index.html.php');
        return $this;
    }

    protected function renderContent()
    {
        if ($this->renderErrors()) {
            return false;
        }
        $actionFile = $this->getCurrentThemeRootPath('controllers/' . $this->response->getController() . '/actions/' . $this->response->getAction() . '.html.php');
        $this->content .= $this->render($actionFile, $this->response->getResponseContent());
        return true;
    }

    protected function renderErrors()
    {
        $errorFile = $this->getCurrentThemeRootPath('assets/errors/' . $this->response->getResponseCode() . '.html.php');
        if (file_exists($errorFile)) {
            $this->content = $this->render($errorFile);
            return true;
        }
        return false;
    }

    public function printContent()
    {
        print $this->content;
        return;
    }
}