<?php

namespace Core\Template\Abstracts;

abstract class Template
{
    public function render($file, array $content = array())
    {
        ob_start();
        extract($content);
        include $file;
        return ob_get_clean();
    }

    abstract public function run($acceptedContentTypes);

    abstract public function getCurrentThemeRootPath($filePath);

    abstract public function getCurrentThemeHttpPath($filePath);
}