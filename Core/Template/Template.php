<?php

namespace Core\Template;

use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;
use Core\Template\Abstracts\Template as AbstractTemplate;
use Core\Template\Traits\Render;
use Helpers\FileSystem;

class Template extends AbstractTemplate
{
    use Render;

    protected $currentTheme = 'default';

    protected $currentThemeRootPath;

    protected $currentThemeHttpPath;

    protected $content;
    /** @var Request */
    protected $request;
    /** @var Response */
    protected $response;

    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->currentThemeRootPath = FileSystem::getPublicRootPath($this->currentTheme);
        $this->currentThemeHttpPath = FileSystem::getPublicHttpPath($this->currentTheme);
    }

    public function run($acceptedContentTypes)
    {
        preg_match_all('#([a-z]+)\/([a-z]+)#', $acceptedContentTypes, $preferredTypes);
        if (isset($preferredTypes[2]) && !empty($preferredTypes[2])) {
            foreach ($preferredTypes[2] as $preferredType) {
                if (method_exists($this, 'render' . $preferredType . 'Content')) {
                    call_user_func(array($this, 'render' . $preferredType . 'Content'));
                    return $this;
                }
            }
        }
        return $this;
    }

    public function getCurrentThemeRootPath($filePath)
    {
        $filePath = trim($filePath, '/');
        return $this->currentThemeRootPath . '/' . $filePath;
    }

    public function getCurrentThemeHttpPath($filePath)
    {
        $filePath = trim($filePath, '/');
        return $this->currentThemeHttpPath . '/' . $filePath;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getResponse()
    {
        return $this->response;
    }
}