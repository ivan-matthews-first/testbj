<?php

namespace Core\Server;

use Helpers\Standard1;

class Server
{
    private $serverData = array();

    public function __construct(array $serverInfo)
    {
        $this->setServerInfoData($serverInfo);
    }

    public function get($name)
    {
        return isset($this->serverData[$name]) ? $this->serverData[$name] : null;
    }

    public function set($name, $value)
    {
        $this->serverData[$name] = Standard1::trim_r($value);
        return $this;
    }

    public function drop($name)
    {
        if (isset($this->serverData[$name])) {
            unset($this->serverData[$name]);
        }
        return $this;
    }

    private function setServerInfoData(array $serverInfo)
    {
        foreach ($serverInfo as $key => $value) {
            $this->set($key, $value);
        }
        return $this;
    }
}