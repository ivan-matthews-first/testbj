<?php

namespace Controllers\Home;

use Core\Database\Model;
use Core\Database\Interfaces\Table;
use Core\Database\Mysqli\Mysqli;

/**
 * Class HomeModel
 * @package Controllers\Home
 * @method static |Table home()
 */
class HomeModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function dropHomeTable()
    {
        $this->home()->drop()->result();
        return $this;
    }

    public function makeHomeTable()
    {
        $this->home()->create('id', 'int(11) not null auto_increment', 'primary key')
            ->create('userName', 'varchar(191) null default null')
            ->create('userEmail', 'varchar(191) null default null')
            ->create('taskBody', 'text null default null', 'fulltext')
            ->create('taskStatus', 'tinyint(1) not null default 0')
            ->create('adminModified', 'tinyint(1) not null default 0')
            ->result();
        return $this;
    }

    public function countTotalTasks()
    {
        return $this->home()->select('count(id) as total')
            ->result()
            ->one()->total;
    }

    public function getAllTasks($limit, $offset, $order, $sort)
    {
        return $this->home()->select()
            ->limit($limit, $offset)
            ->sort($order, $sort)
            ->result()
            ->all(false);
    }

    public function getOneTasks($taskId)
    {
        return $this->home()->select('taskBody', 'taskStatus', 'adminModified')
            ->query('id = :id')
            ->prepare(':id', $taskId)
            ->result()
            ->one(false);
    }

    public function createNewTask($userName, $userEmail, $taskBody, $taskStatus = false)
    {
        return $this->home()->insert('userName', ':userName')
            ->insert('userEmail', ':userEmail')
            ->insert('taskBody', ':taskBody')
            ->insert('taskStatus', ':taskStatus')
            ->prepare(':userName', $userName)
            ->prepare(':userEmail', $userEmail)
            ->prepare(':taskBody', $taskBody)
            ->prepare(':taskStatus', $taskStatus, Mysqli::INTEGER)
            ->result()
            ->id();
    }

    public function updateOldTask($taskId, $taskBody, $taskStatus = true, $adminModified = false)
    {
        return $this->home()->update('taskBody', ':taskBody')
            ->update('taskStatus', ':taskStatus')
            ->update('adminModified', ':adminModified')
            ->query('id = :taskId')
            ->prepare(':taskBody', $taskBody)
            ->prepare(':taskStatus', $taskStatus, Mysqli::INTEGER)
            ->prepare(':taskId', $taskId)
            ->prepare(':adminModified', $adminModified, Mysqli::INTEGER)
            ->result()
            ->rows();
    }
}