<?php

namespace Controllers\Home;

use Controllers\Home\Forms\CreateTaskForm;
use Controllers\Home\Forms\EditTaskForm;
use Core\Controller\Controller;
use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;
use Core\Session\Session;
use Helpers\Functions;

class HomeController extends Controller
{
    private $model;

    private $currentSortingKey;

    private $currentSortingType;

    private $sortingKeys = array(
        'name' => 'userName',
        'email' => 'userEmail',
        'status' => 'taskStatus',
    );

    private $sortingValues = array(
        'name' => 'Имя пользователя',
        'email' => 'Почта пользователя',
        'status' => 'Статус задачи',
    );

    private $total;

    private $limit = 3;

    private $offset;

    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
        $this->model = new HomeModel();
        $this->currentSortingType = $this->setCurrentSortingType($this->request->get('sort'));
        $this->offset = $this->setOffset($this->request->get('offset'));

        $this->response->setController('Home');
    }

    public function index($currentSortingKey = 'name')
    {
        $this->currentSortingKey = isset($this->sortingKeys[$currentSortingKey]) ?
            $this->sortingKeys[$currentSortingKey] : $this->sortingKeys['name'];

        $this->total = $this->model->countTotalTasks();

        $this->response->setAction('index');

        $this->response->setContent('tasks', $this->model->getAllTasks($this->limit, $this->offset, $this->currentSortingKey, $this->currentSortingType));
        $this->response->setContent('sortingKeys', $this->sortingKeys);
        $this->response->setContent('sortingValues', $this->sortingValues);
        $this->response->setContent('currentSortingKey', $currentSortingKey);
        $this->response->setContent('currentSortingType', $this->currentSortingType);
        $this->response->setContent('currentLink', Functions::route('Home.index.sortable', $currentSortingKey)->get());
        $this->response->setContent('totalTasks', $this->total);
        $this->response->setContent('limit', $this->limit);
        $this->response->setContent('offset', $this->offset);
        $this->response->setContent('logged', Session::user('id'));

        return $this;
    }

    public function getCreateTaskForm()
    {

        $this->response->setAction('createTask');

        $form = new CreateTaskForm($this->request, $this->response);
        $form->setFields();
        $form->renderForm();
        return $this;
    }

    public function postCreateTaskForm()
    {
        $this->response->setAction('createTask');

        $form = new CreateTaskForm($this->request, $this->response);
        $form->setFields();
        $form->validateForm();
        if ($form->valid()) {
            $lastId = $this->model->createNewTask(
                $form->getFieldAttribute('userName', 'value'),
                $form->getFieldAttribute('userEmail', 'value'),
                $form->getFieldAttribute('taskBody', 'value')
            );
            if ($lastId) {
                Functions::route('Home.taskCreatedSuccessful')
                    ->redirect($this->response);
                return $this;
            }
        }
        $form->renderForm();
        return $this;
    }

    public function taskCreatedSuccessful()
    {
        $this->response->setAction('taskCreated');
        return $this;
    }

    public function getEditTaskForm($taskId)
    {
        if (!Session::user('id')) {
            $this->response->setResponseCode(403);
            return $this;
        }

        $taskData = $this->model->getOneTasks($taskId);
        if ($taskData) {
            $this->response->setAction('editTask');

            $form = new EditTaskForm($this->request, $this->response);
            $form->setFieldsToGetRequest($taskData['taskBody'], $taskData['taskStatus']);
            $form->renderForm();
            return $this;
        }
        $this->response->setResponseCode(404);
        return $this;
    }

    public function postEditTaskForm($taskId)
    {
        if (!Session::user('id')) {
            $this->response->setResponseCode(403);
            return $this;
        }

        $taskData = $this->model->getOneTasks($taskId);
        if ($taskData) {
            $this->response->setAction('editTask');

            $form = new EditTaskForm($this->request, $this->response);
            $form->setFieldsToPostRequest();
            $form->validateForm();
            if ($form->valid()) {
                $textBody = $form->getFieldAttribute('taskBody', 'value');
                $textStatus = $form->getFieldAttribute('taskStatus', 'value');

                $rows = $this->model->updateOldTask($taskId, $textBody, $textStatus ? true : false,
                    $taskData['adminModified'] ?: ($taskData['taskBody'] !== $textBody ? true : false)
                );
                if ($rows) {
                    Functions::route('Home.index')
                        ->redirect($this->response);
                    return $this;
                }
                $form->setFieldError('form', 'Что-то пошло не так...');
                $form->setFieldError('form', 'Задача не была изменена!');
            }
            $form->renderForm();
            return $this;
        }
        $this->response->setResponseCode(404);
        return $this;
    }
}