<?php

namespace Controllers\Home\Forms;

use Core\Form\Form;
use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;

class CreateTaskForm extends Form
{
    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
    }

    public function setFields()
    {
        $this->field('userName')->text()
            ->setFieldAttribute('placeholder', 'Введите свое имя')
            ->setFieldOption('label', 'Введите свое имя');
        $this->field('userEmail')->email()
            ->setFieldAttribute('placeholder', 'Введите свой E-MAIL адрес')
            ->setFieldOption('label', 'Введите свой E-MAIL адрес');
        $this->field('taskBody')->textarea()
            ->setFieldAttribute('placeholder', 'Заполните тело задачи')
            ->setFieldOption('label', 'Введите текст задачи');
        $this->field(null)->submit();
        return $this;
    }
}