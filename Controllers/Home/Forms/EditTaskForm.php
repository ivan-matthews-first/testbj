<?php

namespace Controllers\Home\Forms;

use Core\Form\Form;
use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;

class EditTaskForm extends Form
{
    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
    }

    public function setFieldsToGetRequest($taskBody, $taskStatus)
    {
        $this->field('taskBody')->textarea()
            ->setFieldAttribute('value', $taskBody)
            ->setFieldAttribute('placeholder', 'Заполните тело задачи')
            ->setFieldOption('label', 'Введите текст задачи');
        $this->field('taskStatus')->checkbox()
            ->setFieldAttribute('value', $taskStatus)
            ->setFieldOption('label', 'Задача выполнена?')
            ->setFieldAttribute('required', false)
            ->setFieldOption('required', false)
            ->setFieldOption('fieldType', 'switch');
        $this->field(null)->submit();
        return $this;
    }

    public function setFieldsToPostRequest()
    {
        $this->field('taskBody')->textarea()
            ->setFieldAttribute('placeholder', 'Заполните тело задачи')
            ->setFieldOption('label', 'Введите текст задачи');
        $this->field('taskStatus')->checkbox()
            ->setFieldOption('label', 'Задача выполнена?')
            ->setFieldAttribute('required', false)
            ->setFieldOption('required', false)
            ->setFieldOption('fieldType', 'switch');
        $this->field(null)->submit();
        return $this;
    }
}