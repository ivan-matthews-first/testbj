<?php

namespace Controllers\Users;

use Controllers\Users\Forms\AuthUserForm;
use Core\Controller\Controller;
use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;
use Core\Session\Session;
use Helpers\Functions;

class UsersController extends Controller
{
    private $model;

    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
        $this->model = new UsersModel();
        $this->response->setController('Users');
    }

    public function getAuthorizeForm()
    {
        if (Session::user('id')) {
            $this->response->setResponseCode(403);
        }
        $this->response->setAction('userAuthorize');
        $form = new AuthUserForm($this->request, $this->response);
        $form->setFields();
        $form->renderForm();
        return $this;
    }

    public function postAuthorizeForm()
    {
        if (Session::user('id')) {
            $this->response->setResponseCode(403);
        }
        $this->response->setAction('userAuthorize');
        $form = new AuthUserForm($this->request, $this->response);
        $form->setFields();
        $form->validateForm();
        if ($form->valid()) {
            $user = $this->model->getUserByLogin($this->request->get('login'));
            $form->validateExistsUser($user);
            $form->validateUserPassword($this->request->get('password'), isset($user['password']) ? $user['password'] : null);
            $form->validateForm();
            if ($form->valid()) {
                $this->authorizeUser($user);
                Functions::route('Home.index')
                    ->redirect($this->response, 302);
                return $this;
            }
        }
        $form->renderForm();
        return $this;
    }

    public function logOut()
    {
        if (!Session::user('id')) {
            $this->response->setResponseCode(403);
        }
        $this->response->setAction('userAuthorize');
        $this->logoutUser();
        Functions::route('Home.index')->redirect($this->response);
        return $this;
    }

    public function logoutUser()
    {
        foreach ($_SESSION['user'] as $key => $value) {
            Session::user($key, null);
        }
        return $this;
    }

    public function authorizeUser(array $userData)
    {
        foreach ($userData as $key => $value) {
            Session::user($key, $value);
        }
        return $this;
    }
}