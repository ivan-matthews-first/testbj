<?php

namespace Controllers\Users\Forms;

use Core\Form\Form;
use Core\Request\Interfaces\Request;
use Core\Response\Interfaces\Response;

class AuthUserForm extends Form
{
    public function __construct(Request $request, Response $response)
    {
        parent::__construct($request, $response);
    }

    public function setFields()
    {
        $this->field('login')->text()
            ->setFieldAttribute('placeholder', 'Введите свой логин')
            ->setFieldOption('label', 'Введите свой логин');
        $this->field('password')->password()
            ->setFieldAttribute('placeholder', 'Введите свой пароль')
            ->setFieldOption('label', 'Введите свой пароль');
        $this->field(null)->submit();
        return $this;
    }

    public function validateExistsUser($userData)
    {
        if (!$userData) {
            $this->setFieldError('login', 'Пользователь не найден!');
        }
        return $this;
    }

    public function validateUserPassword($userPassword, $userPasswordHash)
    {
        if (!password_verify($userPassword, $userPasswordHash)) {
            $this->setFieldError('password', 'Введенный пароль неверен!');
        }
        return $this;
    }
}