<?php

namespace Controllers\Users;

use Core\Database\Model;
use Core\Database\Interfaces\Table;
use Core\Database\Mysqli\Mysqli;

/**
 * Class UsersModel
 * @package Controllers\Users
 * @method static |Table users()
 */
class UsersModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function dropUsersTable()
    {
        $this->users()->drop()->result();
        return $this;
    }

    public function makeUsersTable()
    {
        $this->users()->create('id', 'int(11) not null auto_increment', 'primary key')
            ->create('login', 'varchar(191) null default null', 'unique')
            ->create('password', 'text null default null', 'fulltext')
            ->create('isAdmin', 'tinyint(1) not null default 0')
            ->result();
        return $this;
    }

    public function getUserByLogin($userLogin)
    {
        return $this->users()->select()
            ->query('login = :login')
            ->limit(1)
            ->prepare(':login', $userLogin)
            ->result()
            ->one(false);
    }

    public function createNewAccount($login, $password, $isAdmin = false)
    {
        return $this->users()
            ->insert('login', ':login')
            ->insert('password', ':password')
            ->insert('isAdmin', ':isAdmin')
            ->prepare(':login', $login)
            ->prepare(':password', password_hash($password, PASSWORD_DEFAULT))
            ->prepare(':isAdmin', $isAdmin, Mysqli::INTEGER)
            ->result()
            ->id();
    }
}